import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";
class ProductList extends Component {
  renderProducts = () => {
    const { products, choosenCategories } = this.props;
    return products
      .filter((item) => item.type === choosenCategories)
      .map((item, index) => {
        return (
          <div key={index} className="col-4">
            <ProductItem prod={item} />
          </div>
        );
      });
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row">{this.renderProducts()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.product.productList,
    choosenCategories: state.choosenCategory,
  };
};
export default connect(mapStateToProps)(ProductList);
