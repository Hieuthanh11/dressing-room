import React, { Component } from "react";
import "./model.css";
import { connect } from "react-redux";
class Model extends Component {
  render() {
    return (
      <div
        style={{
          background: " url(./assets/img/background/background_998.jpg)",
        }}
        className="contain"
      >
        <div
          style={{
            background: "url(./assets/img/allbody/bodynew.png)",
          }}
          className="body"
        />
        <div
          style={{
            background: "url(./assets/img/model/test.png)",
            backgroundSize: "cover",
          }}
          className="model"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/bikini_branew.png)",
          }}
          className="bikinitop"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/bikini_pantsnew.png)",
          }}
          className="bikinibottom"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/feet_high_leftnew.png)",
          }}
          className="feetleft"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/feet_high_rightnew.png)",
          }}
          className="feetright"
        />
        <div
          style={{
            backgroundImage: `url(${this.props.model.topclothes})`,
            backgroundSize: "cover",
          }}
          className="bikinitop"
        ></div>
        <div
          style={{
            backgroundImage: `url(${this.props.model.botclothes})`,
            backgroundSize: "cover",
          }}
          className="bikinibottom"
        ></div>
        <div
          style={{
            backgroundImage: `url(${this.props.model.shoes})`,
            backgroundSize: "cover",
          }}
          className="feetStyle"
        ></div>
        <div
          style={{
            backgroundImage: `url(${this.props.model.handbags})`,
            backgroundSize: "cover",
          }}
          className="handbagStyle"
        ></div>
        <div
          style={{
            backgroundImage: `url(${this.props.model.hairstyle})`,
            backgroundSize: "cover",
          }}
          className="hairStyle"
        ></div>
        <div
          style={{
            backgroundImage: `url(${this.props.model.background})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
          }}
          className="backgroundStyle"
        ></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { model: state.model };
};
export default connect(mapStateToProps)(Model);
