import React, { Component } from "react";
import { connect } from "react-redux";

class Category extends Component {
  renderCategories = () => {
    const { categories, chooseCategories } = this.props;
    return categories.map((item, index) => {
      return (
        <button
          onClick={() => this.chooseCategory(item.type)}
          key={index}
          className={
            chooseCategories === item.type
              ? "btn btn-primary"
              : "btn btn-secondary"
          }
        >
          {item.showName}
        </button>
      );
    });
  };

  render() {
    return (
      <div className="btn-group container-fluid mb-4">
        {this.renderCategories()}
      </div>
    );
  }

  chooseCategory = (payload) => {
    this.props.dispatch({
      type: "SET_CATEGORY",
      payload,
    });
  };
}

const mapStateToProps = (state) => {
  return {
    categories: state.category.categoryList,
    chooseCategories: state.choosenCategory,
  };
};

export default connect(mapStateToProps)(Category);
