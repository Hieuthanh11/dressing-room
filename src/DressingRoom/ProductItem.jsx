import React, { Component } from "react";
import { connect } from "react-redux";
class ProductItem extends Component {
  render() {
    const { imgSrc_jpg, name } = this.props.prod;
    return (
      <div className="card mb-3">
        <img src={imgSrc_jpg} alt="product" />
        <div className="card-body">
          <p className="lead">{name}</p>
          <button
            onClick={() => this.chooseCloth(this.props.prod)}
            className="btn btn-success"
          >
            Thử
          </button>
        </div>
      </div>
    );
  }

  chooseCloth = (cloth) => {
    this.props.dispatch({
      type: "SET_CLOTH",
      payload: {
        type: cloth.type,
        img: cloth.imgSrc_png,
      },
    });
  };
}

export default connect()(ProductItem);
