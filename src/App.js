import Home from "./DressingRoom/Home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
